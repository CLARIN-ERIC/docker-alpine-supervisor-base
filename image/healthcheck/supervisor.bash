#!/usr/bin/env bash

(! supervisorctl -u sysops -p "${SUPERVISOR_PASSWORD}" status | grep -v RUNNING &> /dev/null) || exit 1
