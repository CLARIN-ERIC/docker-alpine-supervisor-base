#!/bin//bash

set -e

log() {
    printf '%s %s: {"message": "%s"}\n' "$(date +'%Y-%m-%d %H:%M:%S.%N %z')" "entrypoint" "${1}"
}

signal() {
  log "Received SIG${1} signal"
  log "Shutting down supervisord (PID=$supervisord_pid)"
  kill -s "${1}" "$supervisord_pid"
  wait "$supervisord_pid"

  log "Shutting down fluentd (PID=$fluentd_pid)"
  kill -s "${1}" "$fluentd_pid"
  wait "$fluentd_pid"
}

trap "signal 'INT'" SIGINT
trap "signal 'TERM'" SIGTERM
#traps are not supported for the KILL and STOP signals

HELP=0
RUN_TEST=0
VERBOSE=0

#Fluentd monitor logging (disabled by default)
_FLUENTD_MONITOR_LOGGING=${FLUENTD_MONITOR_LOGGING:-0}

#
# Process script arguments
#
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    --verbose)
    VERBOSE=1
    ;;
    --vverbose)
    VERBOSE=2
    ;;
    --vvverbose)
    VERBOSE=3
    ;;
    --test)
    RUN_TEST=1
    ;;
    -h|--help)
    HELP=1
    ;;
    *)
    echo "Unkown option: $key"
    HELP=1
    ;;
esac
shift # past argument or value
done

#
# Show help message and exit script
#
if [ "${HELP}" -eq 1 ]; then
    echo ""
    echo "entrypoint.sh [args]"
    echo ""
    echo "  --verbose        Run fluentd with the info log level instead of the default warn level"
    echo "  --test           Run in test mode. This will automatically shut down when notified about test completion"
    echo ""
    echo "  -h, --help       Show help"
    echo ""
    exit 1
fi

#Make sure log directories exist
mkdir -p /var/log/supervisord /var/log/fluentd

#Set proper permissions to all logrotate configuration files. These files might be added downstream,
#therefore the permissions are updated at runtime
chmod 0644 /etc/logrotate.clarin/*

#
# Start fluentd
#
declare -a FLUENTD_FLAGS
if [ ${VERBOSE} == "0" ]; then
    FLUENTD_FLAGS+=('-q')
elif [ ${VERBOSE} == "2" ]; then
    FLUENTD_FLAGS+=('-v')
elif [ ${VERBOSE} == "3" ]; then
    FLUENTD_FLAGS+=('-vv')
fi
log "Configuration:"
log "  VERBOSE=${VERBOSE}"
log "  FLUENTD FLAGS=${FLUENTD_FLAGS[*]}"
log "  FLUENTD_MONITOR_LOGGING=${_FLUENTD_MONITOR_LOGGING}"

if [ "${_FLUENTD_MONITOR_LOGGING}" -eq 0 ]; then
    log "Disabling fluentd monitor logging"
    sed -i "s/tag fluentd_monitor/#tag fluentd_monitor/g" "/etc/fluentd/fluentd.conf"
else
    log "Enabling fluentd monitor logging"
fi

log "Starting fluentd"
/usr/bin/fluentd \
  -c '/etc/fluentd/fluentd.conf' \
  -p '/fluentd/plugins' \
  "${FLUENTD_FLAGS[@]}" \
  1>/proc/1/fd/1 2>/proc/1/fd/1 &
fluentd_pid=$!


#
# Block until fluentd is started
#

# shellcheck disable=SC2259
while ! echo exit | nc 127.0.0.1 24220 </dev/null ; do
  log 'Waiting for fluentd to start'; sleep "1";
  #if the tests finished while we are waiting for fluentd, the tests must have timedout and we should fail.
  if  [ -f  "/test/done" ]; then
    log 'Tests have finished while waiting on fluentd. Aborting...'
    exit 1
  fi
done
log "Fluentd started, continueing entrypoint script"

#
# Run script
#
if [ "${RUN_TEST}" -eq 1 ]; then
    log "Forking check_test.sh"
    check_test.sh "/var/run/supervisord.pid" &
fi

#
# Initialize configuration
#
INIT_DIR="/init"
if [ -d "${INIT_DIR}" ]; then
    script_count=$(find "${INIT_DIR}" -name "*.sh" | wc -l)
    if [ "${script_count}" -gt 0 ]; then
        log "Initializing"
        # shellcheck disable=SC2231
        for each in ${INIT_DIR}/*.sh ; do
            log "Running ${each}"

            INIT_ERROR=0
            if [ "${VERBOSE}" == "1" ]; then
                bash -x "${each}" >> /init/init.log 2>&1;
                INIT_ERROR=$?
            else
                bash "${each}" >> /init/init.log 2>&1;
                INIT_ERROR=$?
            fi

            #Exit if init script failed
            if [ "${INIT_ERROR}" != 0 ]; then
                echo "Initialization script failed"
                exit 1
            fi
        done

        #
        #If exit was signalled by any of the init script, stop here
        #
        if [ -f "/init/exit" ]; then
            log "Exiting after initialization"
            exit 0
        fi
    else
        log "Skipping initialization, no scripts found in ${INIT_DIR}"
    fi
else
    log "Skipping initialization, ${INIT_DIR} doesn't exist"
fi

#
#Start supervisor
#

if [ "${RUN_TEST}" -eq 1 ]; then
    log "Enabling supervisorctl server for external requests"
    sed -i 's/port = 127.0.0.1:9001/port = 0.0.0.0:9001/g' /etc/supervisord.conf
fi

log "Starting supervisord"
/usr/bin/supervisord -c /etc/supervisord.conf >> /var/log/supervisord/supervisor-startup.log &
supervisord_pid=$!
wait $supervisord_pid
