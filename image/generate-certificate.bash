#!/bin/bash

TOKEN=""
ROLE=""
CN=""
TTL=""
CERT_FILE=""
KEY_FILE=""

DEFAULT_TTL="8760h"
DEFAULT_CERT_FILE="cert.pem"
DEFAULT_KEY_FILE="key.pem"
DEFAULT_CA_FILE="ca.pem"

gencert() {
    _API_ADDR=${API_ADDR:-http://127.0.0.1:8200}
    WORK_FILE="gencert-response.json"

    CURL_OUPUT=$(curl --header "X-Vault-Token: ${TOKEN}" \
           --request POST \
           --data "{\"common_name\": \"${CN}\", \"ttl\": \"${TTL}\"}" \
           "${_API_ADDR}/v1/pki_int/issue/${ROLE}" > "${WORK_FILE}")
    # shellcheck disable=SC2181
    if [ "$?" == "0" ]; then
        CERT=$(jq .data.certificate < "${WORK_FILE}" | tr -d '"')
        echo -e "${CERT}" > "${CERT_FILE}"

        KEY=$(jq .data.private_key < "${WORK_FILE}" | tr -d '"')
        echo -e "${KEY}" > "${KEY_FILE}"

        CA=$(jq .data.issuing_ca < "${WORK_FILE}" | tr -d '"')
        echo -e "${CA}" > "${CA_FILE}"
        exit 0
    else
        echo "Failed to request certificate:"
        echo "${CURL_OUPUT}"
        exit 1
    fi
}

usage() {
    exitcode="$1"

    echo "Usage:"
    echo ""
    echo "generate-certificate <options>"
    echo ""
    echo "Options:"
    echo "  -t <value>, --token=<value>         Vault token"
    echo "  -r <value>, --role=<value>          Vault role"
    echo "  -n <value>, --cn=<value>            Certificate common name"
    echo "  -T <value>, --ttl=<value>           Certificate TTL. Defaults to ${DEFAULT_TTL}"
    echo "  -c <value>, --certfile=<value>      File to write the certificate to. Defaults to ${DEFAULT_CERT_FILE}"
    echo "  -k <value>, --keyfile=<value>       File to write the private key to. Defauts to ${DEFAULT_KEY_FILE}"
    echo ""
    echo "Supported environment variables:"
    echo "  API_ADDR                            The vault api URL. Defaults to http://127.0.0.1:8200"
    echo ""

    exit "${exitcode}"
}

while [ $# -gt 0 ]
do
  case "$1" in
     -c)
    CERT_FILE="$2"
    shift 2
    ;;
    --certfile)
    CERT_FILE="${1#*=}"
    shift 1
    ;;
     -k)
    KEY_FILE="$2"
    shift 2
    ;;
    --keyfile)
    KEY_FILE="${1#*=}"
    shift 1
    ;;
    -t)
    TOKEN="$2"
    shift 2
    ;;
    --token)
    TOKEN="${1#*=}"
    shift 1
    ;;
    -T)
    TOKEN="$2"
    shift 2
    ;;
    --ttl)
    TTL="${1#*=}"
    shift 1
    ;;
    -n)
    CN="$2"
    shift 2
    ;;
    --cn)
    CN="${1#*=}"
    shift 1
    ;;
    -r)
    ROLE="$2"
    shift 2
    ;;
    --role)
    ROLE="${1#*=}"
    shift 1
    ;;
    *)
    echoerr "Unknown argument: $1"
    usage 1
    ;;
  esac
done

#Check for required arguments
ARG_ERROR=0
if [ "${TOKEN}" == "" ]; then
  echo "ERROR: Vault token is required."
  ARG_ERROR=1
fi
if [ "${ROLE}" == "" ]; then
  echo "ERROR: Vault role is required."
  ARG_ERROR=1
fi

if [ "${CN}" == "" ]; then
  echo "ERROR: Certificate common name is required"
  ARG_ERROR=1
fi

#show usage if any required argument is missing
if [ "${ARG_ERROR}" == "1" ]; then
    echo ""
    usage 2
fi

#Check for defaults
if [ "${TTL}" == "" ]; then
  echo "Using default TTL: ${DEFAULT_TTL}"
  TTL="${DEFAULT_TTL}"
fi
if [ "${CERT_FILE}" == "" ]; then
  echo "Using default cert file: ${DEFAULT_CERT_FILE}"
  CERT_FILE="${DEFAULT_CERT_FILE}"
fi
if [ "${KEY_FILE}" == "" ]; then
  echo "Using default key file: ${DEFAULT_KEY_FILE}"
  KEY_FILE="${DEFAULT_KEY_FILE}"
fi

if [ "${CA_FILE}" == "" ]; then
    CA_FILE="${DEFAULT_CA_FILE}"
fi

gencert "$@"

