#!/usr/bin/env bash

printf "[%s] Running healthcheck\n" "$(date)" >> /var/log/healthcheck.log
DIR="/etc/healthcheck.d"
if [ -d "${DIR}" ]; then
    script_count=$(find "${DIR}" -name "*" | wc -l)
    if [ "${script_count}" -gt 0 ]; then
        # shellcheck disable=SC2231
        for each in ${DIR}/* ; do
            printf "[%s]   script:%s " "$(date)" "${each}" >> /var/log/healthcheck.log
            bash "${each}"
            exit=${?}
            printf ", result=%s\n" "${exit}"  >> /var/log/healthcheck.log
            if [ "${exit}" != 0 ]; then
                exit 1
            fi
        done
    fi
fi

exit 0